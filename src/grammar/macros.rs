macro_rules! parse_error
{
    ($tok:expr, $($args:expr),+) =>
    {
        $crate::grammar::error!($($args),+).at_span($tok.as_span())
    };
}

macro_rules! error
{
    ($($args:expr),+) =>
    {
        Error::from(format!("{}:{} {}", file!(), line!(),&format!($($args),+)))
    }
}

macro_rules! expect_rule
{
    ($tok:expr, $rule:pat) =>
    {{
        let tok = $tok;

        match tok.as_rule()
        {
            $rule => Ok(tok),
            _ =>
            {
                Err($crate::grammar::parse_error!(
                    tok, "Expected rule of type {}, but found {:?}",
                    stringify!($rule), tok.as_rule()
                ))
            }
         }
    }}
}

macro_rules! expect_rule_eq
{
    ($tok:expr, $rule:expr) =>
    {{
        let tok = $tok;
        let rule = $rule;

        #[allow(unreachable_patterns)]
        if tok.as_rule() == $rule
        {
            Ok(tok)
        }
        else
        {
            Err($crate::grammar::parse_error!(
                tok, "Expected rule of type {:?}, but found {:?}",
                rule, tok.as_rule()
            ))
        }
    }};
    ($tok:expr, _) => { Ok($tok) };
}

macro_rules! iter_rule
{
    ($iter:expr, $rule:pat) =>
    {{
        match $iter.next()
        {
            Some(inner) =>
            {
                $crate::grammar::expect_rule!(inner, $rule)
            }
            None =>
            {
                Err($crate::grammar::error!(
                    "Expected rule of type {}, but found nothing",
                    stringify!($rule)
                ))
            }
        }
    }}
}

pub(crate) use parse_error;
pub(crate) use error;
pub(crate) use expect_rule;
pub(crate) use expect_rule_eq;
pub(crate) use iter_rule;
