use super::*;

use pest_derive::{Parser};
use pest::iterators::{Pair, Pairs};

mod macros;
pub(crate) use macros::{
    error, parse_error, expect_rule, expect_rule_eq, iter_rule
};

pub(crate) type Token<'a>     = Pair<'a, Rule>;
pub(crate) type TokenIter<'a> = Pairs<'a, Rule>;

#[derive(Parser)]
#[grammar="matthewdown.pest"]
pub(crate) struct MdParser;

pub(crate) fn tokenize_document<'a>(document: &'a str) -> Result<Token<'a>>
{
    use pest::Parser;

    if let Some(parsed) = MdParser::parse(Rule::document, document)?.next()
    {
        Ok(parsed)
    }
    else
    {
        Err(Error::from("Found no document!"))
    }
}

pub(crate) fn tokenize_content<'a>(content: &'a str) -> Result<Token<'a>>
{
    use pest::Parser;

    if let Some(parsed) = MdParser::parse(Rule::content, content)?.next()
    {
        Ok(parsed)
    }
    else
    {
        Err(Error::from("Found no content!"))
    }
}

#[cfg(test)]
mod tests
{
    use super::*;
    use pest::Parser;

    #[test]
    fn test_parse()
    {
        let string = r#"
# Title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
"#;
        MdParser::parse(Rule::document, string).unwrap();
    }

    #[test]
    fn test_tokenize_document()
    {
        let string = r#"
# Title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
"#;

        tokenize_document(string).unwrap();
    }
}
