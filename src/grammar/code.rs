use super::*;

pub struct Code<'a>
{
    text: &'a str
}

impl<'a> TryFrom<Pair<'a, Rule>> for Code<'a>
{
    type Error = Error;

    fn try_from(tok: Pair<'a, Rule>) -> Result<Code<'a>>
    {
        let text;

        unwrap_rule!(
            expect_rule!(tok, Rule::code)?,
            Rule::code_text => text
        );

        Ok(Self { text: text?.as_str() })
    }
}

impl<'a> walker::Inline<'a> for Code<'a>
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.code(self.text)
    }
}
