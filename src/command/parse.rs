use super::*;

pub(crate) struct Parsed<'a>
{
    pub name:    &'a str,
    pub args:    Args<'a>
}

pub(crate) fn parse<'a>(tok_iter: &mut grammar::TokenIter<'a>)
    -> Result<Parsed<'a>>
{
    let name     = grammar::iter_rule!(tok_iter, grammar::Rule::command_name)?
        .as_str();
    let arg_list = grammar::iter_rule!(tok_iter, grammar::Rule::arg_list)?;

    let mut args = Args::default();

    for tok in arg_list.into_inner()
    {
        let arg = grammar::expect_rule!(tok, grammar::Rule::arg)?;
        args.add_arg(arg.as_str())?;
    }

    Ok(Parsed { name, args })
}
