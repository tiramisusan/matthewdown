use super::*;

pub(crate) trait Factory<T>
    where T: ?Sized + for<'a> Boxed<'a>
{
    fn create<'a>(&self, args: &Args<'a>)
        -> Result<<T as Boxed<'a>>::Boxed>;
}

impl<T, C> Factory<T> for C
    where C: Command<T>, T: ?Sized + for<'a> Boxed<'a>
{
    fn create<'a>(&self, args: &Args<'a>)
        -> Result<<T as Boxed<'a>>::Boxed>
    {
        let positional: C::Positional = args.positional()?;
        let named:      C::Named      = args.named()?;

        <Self as Command<T>>::create(self, positional, named)
    }
}

