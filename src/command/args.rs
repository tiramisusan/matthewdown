use super::*;

use std::collections::HashMap;

/// A structure containing arguments parsed from a command.
#[derive(Default)]
pub(crate) struct Args<'a>
{
    /// A list of positional arguments.
    pos:   Vec<&'a str>,

    /// A map of named arguments.
    named: HashMap<&'a str, &'a str>,
}

impl<'a> Args<'a>
{
    /// Deserialize the positional arguments into a structure.
    pub fn positional<D>(&self) -> Result<D>
        where D: serde::Deserialize<'a>
    {
        use serde::de::IntoDeserializer;
        D::deserialize(self.pos.clone().into_deserializer())
    }

    /// Deserialize the named arguments into a structure.
    pub fn named<D>(&self) -> Result<D>
        where D: serde::Deserialize<'a>
    {
        use serde::de::IntoDeserializer;
        D::deserialize(self.named.clone().into_deserializer())
    }

    /// Parse and add an argument from a string.
    ///
    /// The string may contain a named argument, e.g. `"cow=beef"`, or a
    /// positional argument, e.g. `"moo"`.
    ///
    pub fn add_arg(&mut self, string: &'a str) -> Result
    {
        if let Some((k, v)) = string.split_once('=')
        {
            self.named.insert(k.trim(), v.trim());
        }
        else
        {
            if self.named.len() != 0
            {
                return Err(Error::from("Positional argument following named"));
            }

            self.pos.push(string.trim());
        }

        Ok(())
    }
}

