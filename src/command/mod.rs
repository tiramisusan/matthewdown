use super::*;

mod args;
use args::Args;

mod factory;
use factory::Factory;

mod boxed;
pub use boxed::{Inline, Block, Boxed};

mod map;
pub(crate) use map::Map;

mod parse;
pub(crate) use parse::parse;

mod macros;
pub(crate) use macros::impl_trivial_command;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct NoNamed { }

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct NoPositional();

pub trait Command<T>
    where T: ?Sized + for<'a> Boxed<'a>
{
    type Positional: for<'de> serde::Deserialize<'de>;
    type Named:      for<'de> serde::Deserialize<'de>;

    fn create<'a>(&self, positional: Self::Positional, named: Self::Named)
        -> Result<<T as Boxed<'a>>::Boxed>;

    fn name(&self) -> &'static str;
}
