use super::*;

use std::{
    sync::RwLock,
    collections::HashMap
};

pub(crate) struct Map<T>
    where T: ?Sized
{
    by_name: RwLock<HashMap<String, Box<dyn Factory<T>>>>
}

impl<T> Default for Map<T>
    where T: ?Sized
{
    fn default() -> Self
    {
        Self { by_name: RwLock::new(HashMap::new()) }
    }
}

impl<T> Map<T>
    where T: ?Sized + for<'a> Boxed<'a>
{
    pub fn add_command(&self, command: impl Command<T> + Sized + 'static)
    {
        self.by_name.write().unwrap()
            .insert(command.name().to_owned(), Box::new(command));
    }

    pub fn build<'a>(&self, p: parse::Parsed<'a>)
        -> Result<<T as Boxed<'a>>::Boxed>
    {
        let lock = self.by_name.read().unwrap();
        let Some(factory) = lock.get(p.name)
            else
            {
                return Err(grammar::error!("Unknown command {:?}", p.name));
            };

        factory.create(&p.args)
    }
}


