use super::*;

pub trait Boxed<'a> { type Boxed; }

pub struct Inline;
pub struct Block;

impl<'a> Boxed<'a> for Inline
{
    type Boxed = Box<dyn walker::Inline + 'a>;
}

impl<'a> Boxed<'a> for Block
{
    type Boxed = Box<dyn walker::Block + 'a>;
}

