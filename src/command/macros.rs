use super::*;

macro_rules! impl_trivial_command
{
    ($cmd_type:ty, $walk_type:expr, $name:expr) =>
    {
        pub struct Command;
        impl $crate::command::Command<$cmd_type> for Command
        {
            type Positional = $crate::command::NoPositional;
            type Named      = $crate::command::NoNamed;

            fn create<'a>(&self, _: Self::Positional, _: Self::Named)
                -> Result<<$cmd_type as $crate::command::Boxed<'a>>::Boxed>
            {
                Ok(Box::new($walk_type))
            }

            fn name(&self) -> &'static str { $name }
        }
    }
}

pub(crate) use impl_trivial_command;
