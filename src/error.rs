use super::*;

type Boxed = Box<dyn std::error::Error + Sync + Send + 'static>;
type PestErr = pest::error::Error<grammar::Rule>;

#[derive(Debug)]
pub struct Error
{
    error: Boxed,
    pest:  Option<PestErr>
}

pub type Result<T=()> = std::result::Result<T, Error>;

impl Error
{
    fn new_custom<E>(e: E) -> Self where Boxed: From<E>
    {
        eprintln!("ERROR");
        Self { error: Boxed::from(e), pest: None }
    }

    fn new_pest(pest: PestErr) -> Self
    {
        Self { error: Box::new(pest.clone()), pest: Some(pest) }
    }

    pub fn at_span(mut self, span: pest::Span<'_>) -> Self
    {
        if self.pest.is_none()
        {
            let message = format!("{}", self.error);
            let err = PestErr::new_from_span(
                pest::error::ErrorVariant::CustomError { message },
                span
            );
            eprintln!("{}", err);
            self.pest = Some(err);
        }

        self
    }

    pub fn at_pos(mut self, pos: pest::Position<'_>) -> Self
    {
        if self.pest.is_none()
        {
            let message = format!("{}", self.error);
            let err = PestErr::new_from_pos(
                pest::error::ErrorVariant::CustomError { message },
                pos
            );
            eprintln!("{}", err);
            self.pest = Some(err);
        }

        self
    }
}

impl From<PestErr> for Error
{
    fn from(e: PestErr) -> Error { Error::new_pest(e) }
}

impl From<String> for Error
{
    fn from(s: String) -> Error { Error::new_custom(s) }
}

impl From<&str> for Error
{
    fn from(s: &str) -> Error { Error::new_custom(s) }
}

impl From<std::fmt::Error> for Error
{
    fn from(e: std::fmt::Error) -> Error { Error::new_custom(e) }
}

impl From<std::num::ParseIntError> for Error
{
    fn from(e: std::num::ParseIntError) -> Error { Error::new_custom(e) }
}

impl From<std::io::Error> for Error
{
    fn from(e: std::io::Error) -> Error { Error::new_custom(e) }
}



impl std::fmt::Display for Error
{
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        match &self.pest
        {
            Some(pest) => pest.fmt(fmt),
            None       => self.error.fmt(fmt)
        }
    }
}

impl std::error::Error for Error
{
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)>
    {
        Some(self.error.as_ref())
    }
}

