use super::*;

#[derive(PartialEq)]
pub enum HasContent
{
    No,
    First,
    Last
}

pub trait Inline
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>;

    fn rule(&self) -> grammar::Rule;

    fn has_content(&self) -> HasContent { HasContent::No }
}

impl Inline for Box<dyn Inline>
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        self.as_ref().create(tok_iter)
    }

    fn rule(&self) -> grammar::Rule
    {
        self.as_ref().rule()
    }

    fn has_content(&self) -> HasContent { self.as_ref().has_content() }
}

pub(super) fn parse<'a>(block_type: &impl Inline, tok: grammar::Token<'a>)
    -> Result<inlines::Container<'a>>
{
    let has_content = block_type.has_content();
    let mut inner = grammar::expect_rule_eq!(tok, block_type.rule())?
        .into_inner();

    let mut content = None;

    if has_content == HasContent::First
    {
        content = Some(grammar::iter_rule!(
            inner,
            grammar::Rule::code_block_content |
            grammar::Rule::content            |
            grammar::Rule::no_content         |
            grammar::Rule::content_line
        )?);
    }

    let inline = block_type.create(&mut inner)?;

    if has_content == HasContent::Last
    {
        content = Some(grammar::iter_rule!(
            inner,
            grammar::Rule::code_block_content |
            grammar::Rule::content            |
            grammar::Rule::no_content         |
            grammar::Rule::content_line
        )?);
    }

    Ok(inlines::Container::new(inline, content))
}
