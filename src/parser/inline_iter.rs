use super::*;

pub(crate) struct InlineIter<'a>
{
    parser:      &'a parser::Parser,
    toks:        grammar::TokenIter<'a>,
    latest_span: Option<pest::Span<'a>>
}

impl<'a> Iterator for InlineIter<'a>
{
    type Item = Result<inlines::Container<'a>>;

    fn next(&mut self) -> Option<Result<inlines::Container<'a>>>
    {
        let tok  = self.toks.next()?;
        self.latest_span = Some(tok.as_span());

        Some(
            self.parser.parse_inline(tok)
                .map_err(|e| self.annotate_error(e))
        )
    }
}

impl<'a> InlineIter<'a>
{
    pub fn new(parser: &'a parser::Parser, toks: grammar::TokenIter<'a>) -> Self
    {
        Self { parser, toks, latest_span: None }
    }

    pub fn parser(&self) -> &'a Parser { self.parser }

    pub fn annotate_error(&self, e: Error) -> Error
    {
        if let Some(span) = self.latest_span
        {
            e.at_span(span)
        }
        else
        {
            e
        }
    }
}
