use super::*;

pub trait Block
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>;

    fn rule(&self) -> grammar::Rule;
}

impl Block for Box<dyn Block>
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        self.as_ref().create(tok_iter)
    }

    fn rule(&self) -> grammar::Rule
    {
        self.as_ref().rule()
    }
}

fn indent_depth<'a>(pair: grammar::Token<'a>) -> Result<usize>
{
    Ok(grammar::expect_rule!(pair, grammar::Rule::block_indent)?.as_str().len())
}

pub(super) fn parse<'a>(
    block_type: &impl Block,
    tok:        grammar::Token<'a>,
)
    -> Result<blocks::Container<'a>>
{
    let mut iter = grammar::expect_rule_eq!(tok, block_type.rule())?
        .into_inner();

    let indent  = grammar::iter_rule!(iter, grammar::Rule::block_indent)?;
    let block   = block_type.create(&mut iter)?;
    let content = grammar::iter_rule!(
        iter,
        grammar::Rule::code_block_content |
        grammar::Rule::content            |
        grammar::Rule::no_content         |
        grammar::Rule::content_line
    )?;

    let indent_depth = indent_depth(indent)?;

    Ok(blocks::Container::new(block, content, indent_depth))
}
