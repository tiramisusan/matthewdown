use super::*;

mod inline;
pub use inline::{Inline, HasContent};

mod inline_iter;
pub(crate) use inline_iter::InlineIter;

mod block;
pub use block::Block;

mod block_iter;
pub(crate) use block_iter::BlockIter;

use std::{
    collections::HashMap,
    sync::Arc
};

/// A Matthewdown parser.
pub struct Parser
{
    inline_types:    HashMap<grammar::Rule, Box<dyn Inline>>,
    block_types:     HashMap<grammar::Rule, Box<dyn Block>>,
    inline_commands: Arc<command::Map<command::Inline>>,
    block_commands:  Arc<command::Map<command::Block>>,
}

impl Default for Parser
{
    fn default() -> Self
    {
        let mut rtn = Self::blank();

        rtn = inlines::add_to_parser(rtn);
        rtn = blocks::add_to_parser(rtn);

        rtn
    }
}

impl Parser
{
    /// Get a parser, but with no types or commands.
    ///
    /// This is only useful if you are doing something weird.
    /// To get a usable parser, use `Parser::default()`.
    ///
    pub fn blank() -> Self
    {
        Parser
        {
            inline_types:    Default::default(),
            block_types:     Default::default(),
            inline_commands: Arc::new(Default::default()),
            block_commands:  Arc::new(Default::default())
        }
    }

    /// Add an additional inline type to the parser.
    ///
    /// # Arguments
    ///
    ///  * `inline` - The inline type to add. This produces instances of
    ///               inlines parsed directly from grammar rules.
    ///
    pub fn with_inline_type(mut self, inline: impl Inline + Sized + 'static)
        -> Self
    {
        self.inline_types.insert(inline.rule(), Box::new(inline));
        self
    }

    /// Add an additional block type to the parser.
    ///
    /// # Arguments
    ///
    ///  * `block` - The block type to add. This produces instances of blocks
    ///              parsed directly from grammar rules.
    ///
    pub fn with_block_type(mut self, block: impl Block + Sized + 'static)
        -> Self
    {
        self.block_types.insert(block.rule(), Box::new(block));
        self
    }

    /// Add a new inline command to this parser.
    ///
    /// The new command can be invoked with the syntax `$name(args)[content]`
    /// in an inline context, and can produce inline content.
    ///
    /// # Arguments
    ///
    ///  * `command` - The command type to add. This must produce instances of
    ///                inlines parameterised with command arguments.
    ///
    /// # Examples
    ///
    /// A simple custom command that performs addition.
    ///
    /// ```
    /// struct CustomCommand;
    /// struct CustomAdder(u32, u32);
    ///
    /// impl matthewdown::command::Command<matthewdown::command::Inline>
    ///     for CustomCommand
    /// {
    ///     type Positional = (String, String);
    ///     type Named      = matthewdown::command::NoNamed;
    ///
    ///     fn create<'a>(&self, positional: Self::Positional, _: Self::Named)
    ///         -> matthewdown::MdResult<
    ///             Box<dyn matthewdown::walker::Inline + 'a>>
    ///     {
    ///         let a: u32 = positional.0.parse()?;
    ///         let b: u32 = positional.1.parse()?;
    ///         Ok(Box::new(CustomAdder(a, b)))
    ///     }
    ///
    ///     fn name(&self) -> &'static str { "add" }
    /// }
    ///
    /// impl matthewdown::walker::Inline for CustomAdder
    /// {
    ///     fn visit(
    ///         &self,
    ///         visitor: &mut dyn matthewdown::Visitor,
    ///         handler: &mut matthewdown::walker::InlineHandler
    ///     )
    ///         -> matthewdown::MdResult
    ///     {
    ///         handler.inject(
    ///             visitor,
    ///             &format!("{} + {} = {}", self.0, self.1, self.0 + self.1)
    ///         )
    ///     }
    /// }
    ///
    /// let parser = matthewdown::Parser::default()
    ///     .with_inline_command(CustomCommand);
    ///
    /// use matthewdown::Visitor;
    /// let result = matthewdown::HtmlVisitor::new()
    ///     .with_parsed_str(&parser, "It's maths time: $add(1, 2)")
    ///     .unwrap()
    ///     .into_output();
    ///
    /// assert_eq!(result, "<p>It's maths time: 1 + 2 = 3</p>");
    /// ```
    ///
    pub fn with_inline_command<C>(self, command: C) -> Self
        where C: command::Command<command::Inline> + Sized + 'static
    {
        self.inline_commands.add_command(command);
        self
    }

    /// Add a new block command to this parser.
    ///
    /// The new command can be invoked with the syntax `$$name(args)[content]`
    /// in a block context, and produces a block.
    ///
    /// # Arguments
    ///
    ///  * `command` - The command type to add. This must produce instances of
    ///                blocks parameterised with command arguments.
    ///
    pub fn with_block_command<C>(self, command: C) -> Self
        where C: command::Command<command::Block> + Sized + 'static
    {
        self.block_commands.add_command(command);
        self
    }

    pub(crate) fn visit_str<V>(&self, input: &str, visitor: &mut V) -> Result
        where V: Visitor
    {
        use crate::walker::Walker;
        self.parse_document(input)?
            .walk(visitor)
    }

    pub(crate) fn inline_command_map(&self)
        -> Arc<command::Map<command::Inline>>
    {
        self.inline_commands.clone()
    }

    pub(crate) fn block_command_map(&self)
        -> Arc<command::Map<command::Block>>
    {
        self.block_commands.clone()
    }

    pub(crate) fn parse_document<'a>(&'a self, document: &'a str)
        -> Result<BlockIter<'a>>
    {
        let toks = grammar::tokenize_document(document)?.into_inner();
        Ok(block_iter::BlockIter::new(self, toks))
    }

    pub(crate) fn parse_content<'a>(&'a self, content: &'a str)
        -> Result<InlineIter<'a>>
    {
        Ok(self.iter_inline(grammar::tokenize_content(content)?))
    }

    pub(crate) fn iter_inline<'a>(&'a self, tok: grammar::Token<'a>)
        -> InlineIter<'a>
    {
        let toks = tok.into_inner();
        inline_iter::InlineIter::new(self, toks)
    }

    fn parse_block<'a>(&'a self, tok: grammar::Token<'a>)
        -> Result<blocks::Container<'a>>
    {
        let Some(inner) = grammar::expect_rule!(tok, grammar::Rule::block)?
            .into_inner().next()
            else
            {
                return Err(grammar::error!("Empty block"));
            };

        let Some(block_type) = self.block_types.get(&inner.as_rule())
            else
            {
                return Err(grammar::parse_error!(
                    inner, "Unknown block type: {:?}", inner.as_rule()
                ));
            };

        block::parse(block_type, inner)
    }

    fn parse_inline<'a>(&'a self, tok: grammar::Token<'a>)
        -> Result<inlines::Container<'a>>
    {
        if let Some(inline_type) = self.inline_types.get(&tok.as_rule())
        {
            inline::parse(inline_type, tok)
        }
        else
        {
            Err(grammar::parse_error!(
                tok, "Unknown inline type: {:?}", tok.as_rule()
            ))
        }
    }
}
