use super::*;

pub(crate) struct BlockIter<'a>
{
    parser:      &'a parser::Parser,
    toks:        grammar::TokenIter<'a>,
    latest_span: Option<pest::Span<'a>>
}

impl<'a> Iterator for BlockIter<'a>
{
    type Item = Result<blocks::Container<'a>>;

    fn next(&mut self) -> Option<Result<blocks::Container<'a>>>
    {
        let tok = self.toks.next()?;
        self.latest_span = Some(tok.as_span());

        if tok.as_rule() != grammar::Rule::EOI
        {
            Some(
                self.parser.parse_block(tok)
                    .map_err(|e| self.annotate_error(e))
            )
        }
        else
        {
            None
        }
    }
}

impl<'a> BlockIter<'a>
{
    pub fn new(parser: &'a parser::Parser, toks: grammar::TokenIter<'a>) -> Self
    {
        Self { parser, toks, latest_span: None }
    }

    pub fn parser(&self) -> &'a Parser { self.parser }

    pub fn annotate_error(&self, e: Error) -> Error
    {
        if let Some(span) = self.latest_span
        {
            e.at_span(span)
        }
        else
        {
            e
        }
    }
}
