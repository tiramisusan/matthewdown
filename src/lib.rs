mod grammar;
pub mod walker;
pub mod visitor;
mod error;
pub mod parser;
mod inlines;
mod blocks;
pub mod command;

/// A convenience function to simply convert Matthewdown to HTML.
///
/// # Arguments
///
///  * `input` - A string containing a Matthewdown document.
///
/// # Examples
///
/// ```
/// let html = matthewdown::htmlify_str("Hello World!").unwrap();
/// assert_eq!(html, "<p>Hello World!</p>");
/// ```
///
pub fn htmlify_str(input: &str) -> Result<String>
{
    let rtn = HtmlVisitor::new()
        .with_visited_str(input)?
        .into_output();

    Ok(rtn)
}

/// Visit a Matthewdown string with a visitor.
///
/// Note that it may be more ergonomic to use `Visitor::with_visited_str`, or
/// `Visitor::with_parsed_str`.
///
/// # Arguments
///
///  * `input` - A string containing a Matthewdown document.
///  * `visitor` - An implementor of the `Visitor` trait to visit the document.
///
/// # Examples
///
/// ```
/// let mut visitor = matthewdown::HtmlVisitor::new();
/// matthewdown::visit_str("# Title", &mut visitor);
///
/// assert_eq!(visitor.into_output(), "<h1>Title</h1>");
/// ```
///
pub fn visit_str<V>(input: &str, visitor: &mut V) -> Result
    where V: Visitor
{
    parser::Parser::default().visit_str(input, visitor)
}

// Expose visitor types
pub use visitor::{
    HtmlVisitor, MetadataVisitor, TextVisitor, NullVisitor, HtmlConfig, Visitor
};

// Expose parser type
pub use parser::Parser;

use error::{Error, Result};

/// A Matthewdown result.
pub type MdResult<T=()> = Result<T>;
/// A Matthewdown error.
pub type MdError = Error;
