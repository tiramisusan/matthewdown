use super::*;

pub struct Parser;
pub struct Paragraph;

impl parser::Block for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(Paragraph))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::paragraph }
}

impl walker::Block for Paragraph
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Paragraph)
    }
}
