use super::*;

pub struct Metadata;

impl walker::Block for Metadata
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Metadata)
    }
}

command::impl_trivial_command!(command::Block, Metadata, "metadata");
