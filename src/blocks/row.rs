use super::*;

pub struct Row;

impl walker::Block for Row
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Row)
    }

    fn group_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Table)
    }
}

command::impl_trivial_command!(command::Block, Row, "row");
