use super::*;

use std::sync::Arc;

pub struct Parser
{
    command_map: Arc<command::Map<command::Block>>
}

impl Parser
{
    pub(crate) fn new(command_map: Arc<command::Map<command::Block>>) -> Self
    {
        Self { command_map }
    }
}

impl parser::Block for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        self.command_map.build(command::parse(tok_iter)?)
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::block_command }
}
