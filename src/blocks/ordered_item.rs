use super::*;

pub struct Parser;
pub struct OrderedItem;

impl parser::Block for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(OrderedItem))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::ordered_item }
}

impl walker::Block for OrderedItem
{
    fn group_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::OrderedList)
    }

    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::OrderedItem)
    }
}
