use super::*;

mod container;
pub(crate) use container::Container;

mod unordered_item;
mod block_command;
mod ordered_item;
mod block_attrs;
mod code_block;
mod paragraph;
mod metadata;
mod heading;
mod photo;
mod cell;
mod row;
mod nb;

pub(crate) fn add_to_parser(parser: parser::Parser) -> parser::Parser
{
    let cmd_map = parser.block_command_map();

    parser
        .with_block_type(unordered_item::Parser)
        .with_block_type(block_command::Parser::new(cmd_map))
        .with_block_type(ordered_item::Parser)
        .with_block_type(code_block::Parser)
        .with_block_type(paragraph::Parser)
        .with_block_type(heading::Parser)
        .with_block_command(block_attrs::Command)
        .with_block_command(metadata::Command)
        .with_block_command(photo::Command)
        .with_block_command(cell::Command)
        .with_block_command(row::Command)
        .with_block_command(nb::Command)
}
