use super::*;

pub struct Nb;

impl walker::Block for Nb
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Nb)
    }
}

command::impl_trivial_command!(command::Block, Nb, "nb");
