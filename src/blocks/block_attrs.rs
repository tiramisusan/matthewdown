use super::*;

use std::collections::HashMap;

pub struct Command;
pub struct BlockAttrs
{
    attrs: HashMap<String, String>
}

impl walker::Block for BlockAttrs
{
    fn visit_post(
        &self,
        _:       &mut dyn visitor::Visitor,
        handler: &mut walker::BlockHandler
    )
        -> Result
    {
        for (k, v) in self.attrs.iter()
        {
            handler.set_next_block_attr(k, v)
        }

        Ok(())
    }
}

impl command::Command<command::Block> for Command
{
    type Positional = command::NoPositional;
    type Named      = HashMap<String, String>;

    fn create<'a>(&self, _: Self::Positional, attrs: HashMap<String, String>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(BlockAttrs { attrs }))
    }

    fn name(&self) -> &'static str { "block-attrs" }
}
