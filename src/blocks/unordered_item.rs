use super::*;

pub struct Parser;
pub struct UnorderedItem;

impl parser::Block for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(UnorderedItem))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::unordered_item }
}

impl walker::Block for UnorderedItem
{
    fn group_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::UnorderedList)
    }

    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::UnorderedItem)
    }
}
