use super::*;

struct Photo
{
    href: String
}

impl walker::Block for Photo
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Photo(self.href.clone()))
    }
}

pub struct Command;

impl command::Command<command::Block> for Command
{
    type Positional = (String,);
    type Named      = command::NoNamed;

    fn create<'a>(&self, args: Self::Positional, _: Self::Named)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(Photo { href: args.0 }))
    }

    fn name(&self) -> &'static str { "photo" }
}
