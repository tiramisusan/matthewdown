use super::*;

pub struct Cell;

impl walker::Block for Cell
{
    fn block_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Cell)
    }
}

command::impl_trivial_command!(command::Block, Cell, "cell");
