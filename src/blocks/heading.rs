use super::*;

pub struct Parser;
pub struct Heading
{
    level: usize
}

impl parser::Block for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block>>
    {
        let level = grammar::iter_rule!(tok_iter, grammar::Rule::heading_level)?
            .as_str()
            .chars()
            .filter(|&c| c == '#')
            .count();

        Ok(Box::new(Heading { level }))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::heading }
}

impl walker::Block for Heading
{
    fn inline_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Heading(self.level))
    }
}
