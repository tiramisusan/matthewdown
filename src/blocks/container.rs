use super::*;

pub(crate) struct Container<'a>
{
    block:   Box<dyn walker::Block + 'a>,
    content: grammar::Token<'a>,
    depth:   usize
}

impl<'a> Container<'a>
{
    pub fn new(
        block:   Box<dyn walker::Block + 'a>,
        content: grammar::Token<'a>,
        depth:   usize
    )
        -> Self
    {
        Self { block, content, depth }
    }

    pub fn block(&self) -> &dyn walker::Block { &self.block }

    pub fn block_mut(&mut self) -> &mut dyn walker::Block { &mut self.block }

    pub fn content(&self) -> Option<grammar::Token<'a>>
    {
        Some(self.content.clone())
    }

    pub fn depth(&self) -> usize { self.depth }
}
