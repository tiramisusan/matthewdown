use super::*;

pub struct Parser;

#[derive(Default)]
pub struct CodeBlock
{
    source: Option<String>
}

impl parser::Block for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(CodeBlock::default()))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::code_block }
}

impl walker::Block for CodeBlock
{
    fn inline_region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::CodeBlock)
    }

    fn visit_post(
        &self,
        visitor: &mut dyn visitor::Visitor,
        handler: &mut walker::BlockHandler
    )
        -> Result
    {
        if let Some(source) = &self.source
        {
            let name = <str as AsRef<std::path::Path>>::as_ref(source)
                .file_name()
                .and_then(|p| p.to_str())
                .unwrap_or(source);
            let href       = source;

            // TODO: Escape markdown
            let inject = &format!("From [{}]({}).", name, href);
            handler.inject(&mut visitor::SizedDyn(visitor), &inject)?;
        }

        Ok(())
    }

    fn set_attr(&mut self, key: &str, value: &str) -> Result
    {
        match key
        {
            "source" => self.source = Some(value.to_owned()),
            _ => return Err(Error::from(format!("Unknown attr {:?}", value)))
        }

        Ok(())
    }
}
