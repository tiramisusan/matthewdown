use super::*;

pub struct Small;

impl walker::Inline for Small
{
    fn region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Small)
    }
}

command::impl_trivial_command!(command::Inline, Small, "small");
