use super::*;

mod container;
pub(crate) use container::Container;

mod inline_command;
mod code_multiline;
mod italic_toggle;
mod bold_toggle;
mod escape_seq;
mod line_break;
mod small;
mod link;
mod code;
mod text;
mod br;

pub(crate) fn add_to_parser(parser: parser::Parser) -> parser::Parser
{
    let cmd_map = parser.inline_command_map();

    parser
        .with_inline_type(inline_command::Parser::new(cmd_map))
        .with_inline_type(code_multiline::Parser)
        .with_inline_type(italic_toggle::Parser)
        .with_inline_type(bold_toggle::Parser)
        .with_inline_type(escape_seq::Parser)
        .with_inline_type(line_break::Parser)
        .with_inline_type(link::Parser)
        .with_inline_type(code::Parser)
        .with_inline_type(text::Parser)
        .with_inline_command(small::Command)
        .with_inline_command(br::Command)
}
