use super::*;

pub struct BoldToggle;
pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        Ok(Box::new(BoldToggle))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::bold_toggle }
}

impl<'a> walker::Inline for BoldToggle
{
    fn visit(
        &self,
        visitor: &mut dyn visitor::Visitor,
        handler: &mut walker::InlineHandler
    )
        -> Result
    {
        handler.toggle_region(visitor, visitor::Region::Bold)
    }
}
