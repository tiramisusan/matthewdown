use super::*;

pub struct Link<'a>
{
    href: &'a str
}

impl<'a> walker::Inline for Link<'a>
{
    fn region(&self) -> Option<visitor::Region>
    {
        Some(visitor::Region::Link(String::from(self.href)))
    }
}

pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        use grammar::iter_rule;
        let href = iter_rule!(tok_iter, grammar::Rule::link_ref)?
            .as_str();

        Ok(Box::new(Link { href }))
    }

    fn has_content(&self) -> parser::HasContent { parser::HasContent::First }

    fn rule(&self) -> grammar::Rule { grammar::Rule::link }
}

