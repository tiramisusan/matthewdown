use super::*;

pub(crate) struct Container<'a>
{
    inline:  Box<dyn walker::Inline + 'a>,
    content: Option<grammar::Token<'a>>
}

impl<'a> Container<'a>
{
    pub fn new(
        inline:  Box<dyn walker::Inline + 'a>,
        content: Option<grammar::Token<'a>>
    )
        -> Self
    {
        Self { inline, content }
    }

    pub fn content(&self) -> Option<grammar::Token<'a>>
    {
        self.content.clone()
    }

    pub fn inline(&self) -> &dyn walker::Inline { &self.inline }
}
