use super::*;

pub struct ItalicToggle;
pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        Ok(Box::new(ItalicToggle))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::italic_toggle }
}

impl<'a> walker::Inline for ItalicToggle
{
    fn visit(
        &self,
        visitor: &mut dyn visitor::Visitor,
        handler: &mut walker::InlineHandler
    )
        -> Result
    {
        handler.toggle_region(visitor, visitor::Region::Italic)
    }
}
