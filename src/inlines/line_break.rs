use super::*;

pub struct LineBreak;

impl walker::Inline for LineBreak
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.text(" ")
    }
}

pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, _: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        Ok(Box::new(LineBreak))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::line_break }
}
