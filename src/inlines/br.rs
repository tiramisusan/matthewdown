use super::*;

pub struct Br;

impl walker::Inline for Br
{
    fn visit(
        &self,
        visitor: &mut dyn visitor::Visitor,
        _:       &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.line_break()
    }
}

command::impl_trivial_command!(command::Inline, Br, "br");
