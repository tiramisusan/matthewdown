use super::*;

pub struct EscapeSeq(char);
pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        let tok = grammar::iter_rule!(tok_iter, grammar::Rule::escape_char)?;

        if let Some(ch) = tok.as_str().chars().next()
        {
            Ok(Box::new(EscapeSeq(ch)))
        }
        else
        {
            Err(grammar::parse_error!(tok, "Zero-length escape sequence."))
        }
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::escape_seq }
}

impl walker::Inline for EscapeSeq
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.text(&self.0.to_string())
    }
}
