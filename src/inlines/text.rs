use super::*;

pub struct Text<'a>(&'a str);
pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        let text_str = grammar::iter_rule!(tok_iter, grammar::Rule::text_str)?;

        Ok(Box::new(Text(text_str.as_str())))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::text }
}

impl<'a> walker::Inline for Text<'a>
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.text(self.0)
    }
}
