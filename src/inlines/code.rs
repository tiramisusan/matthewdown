use super::*;

pub struct Parser;
pub struct Code<'a>
{
    text: &'a str
}

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        let text = grammar::iter_rule!(tok_iter, grammar::Rule::code_str)?
            .as_str();

        Ok(Box::new(Code { text }))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::code }
}

impl<'a> walker::Inline for Code<'a>
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.code(self.text)
    }
}
