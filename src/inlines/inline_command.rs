use super::*;

use std::sync::Arc;

pub struct Parser
{
    command_map: Arc<command::Map<command::Inline>>
}

impl Parser
{
    pub(crate) fn new(command_map: Arc<command::Map<command::Inline>>) -> Self
    {
        Self { command_map }
    }
}

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        self.command_map.build(command::parse(tok_iter)?)
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::inline_command }

    fn has_content(&self) -> parser::HasContent { parser::HasContent::Last }
}
