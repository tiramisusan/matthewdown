use super::*;

pub struct CodeMultiline<'a>
{
    text: &'a str
}

impl<'a> walker::Inline for CodeMultiline<'a>
{
    fn visit(
        &self,
        visitor:  &mut dyn visitor::Visitor,
        _handler: &mut walker::InlineHandler
    )
        -> Result
    {
        visitor.code(self.text)
    }
}

pub struct Parser;

impl parser::Inline for Parser
{
    fn create<'a>(&self, tok_iter: &mut grammar::TokenIter<'a>)
        -> Result<Box<dyn walker::Inline + 'a>>
    {
        let text = grammar::iter_rule!(
                tok_iter, grammar::Rule::code_multiline_str)?
            .as_str();

        Ok(Box::new(CodeMultiline { text }))
    }

    fn rule(&self) -> grammar::Rule { grammar::Rule::code_multiline }
}

