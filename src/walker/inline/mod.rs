use super::*;

mod handler;
pub use handler::InlineHandler;

pub trait Inline
{
    /// Get the region for the contents of this inline.
    fn region(&self) -> Option<visitor::Region> { None }

    /// Perform an action after the content of this inline.
    ///
    /// # Parameters
    ///
    ///  * `visitor` - The visitor to visit the actions upon.
    ///  * `handler` - The current inline handling context.
    ///
    fn visit(&self, visitor: &mut dyn Visitor, handler: &mut InlineHandler)
        -> Result
    {
        let _ = visitor;
        let _ = handler;

        Ok(())
    }
}

impl<'a> Inline for Box<dyn Inline + 'a>
{
    fn region(&self)  -> Option<visitor::Region>
    {
        self.as_ref().region()
    }

    fn visit(&self, visitor: &mut dyn Visitor, handler: &mut InlineHandler)
        -> Result
    {
        self.as_ref().visit(visitor, handler)
    }
}
