use super::*;

pub struct InlineHandler<'a>
{
    parser:       &'a parser::Parser,
    region_stack: Vec<visitor::Region>
}

impl<'a> InlineHandler<'a>
{
    pub fn new(parser: &'a parser::Parser) -> Self
    {
        Self { parser, region_stack: Default::default() }
    }

    fn pop_region(&mut self, visitor: &mut dyn Visitor)
        -> Result<Option<visitor::Region>>
    {
        if let Some(popped) = self.region_stack.pop()
        {
            visitor.end(&popped)?;
            Ok(Some(popped))
        }
        else
        {
            Ok(None)
        }
    }

    fn push_region(
        &mut self,
        visitor: &mut dyn Visitor,
        region:  visitor::Region
    )
        -> Result
    {
        visitor.begin(&region)?;
        self.region_stack.push(region);

        Ok(())
    }

    pub fn toggle_region(
        &mut self,
        visitor: &mut dyn Visitor,
        region:  visitor::Region
    )
        -> Result
    {
        if self.region_stack.iter().any(|r| r == &region)
        {
            let mut to_push = Vec::new();

            while let Some(r) = self.pop_region(visitor)?
            {
                if r == region { break; }
                to_push.push(r);
            }

            for r in to_push.into_iter().rev()
            {
                self.push_region(visitor, r)?;
            }
        }
        else
        {
            self.push_region(visitor, region)?;
        }

        Ok(())
    }

    pub(crate) fn handle(
        &mut self,
        visitor:   &mut dyn Visitor,
        container: inlines::Container<'a>
    )
        -> Result
    {
        let region = container.inline().region();
        let has_region = region.is_some();

        if let Some(r) = region
        {
            self.push_region(visitor, r)?;
        }

        if let Some(content) = container.content()
        {
            self.parser.iter_inline(content).walk(visitor)?;
        }

        container.inline().visit(visitor, self)?;

        if has_region
        {
            self.pop_region(visitor)?;
        }

        Ok(())
    }

    /// Inject a string containing inline Matthewdown content.
    ///
    /// The string can only contain inline elements, not blocks!
    ///
    pub fn inject(&mut self, visitor: &mut dyn Visitor, md: &str)
        -> Result
    {
        self.parser.parse_content(md)?.walk(visitor)?;

        Ok(())
    }

    pub fn end_all(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        while self.pop_region(visitor)?.is_some() { }

        Ok(())
    }
}
