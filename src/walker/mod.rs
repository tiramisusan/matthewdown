use super::*;

mod block;
pub use block::{Block, BlockHandler};

mod inline;
pub use inline::{Inline, InlineHandler};

pub trait Walker
{
    fn walk(&mut self, visitor: &mut dyn Visitor) -> Result;
}

impl<'a> Walker for parser::InlineIter<'a>
{
    fn walk(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        let mut inline_handler = InlineHandler::new(self.parser());

        while let Some(inline) = self.next()
        {
            inline_handler.handle(visitor, inline?)
                .map_err(|e| self.annotate_error(e))?;
        }

        inline_handler.end_all(visitor)
            .map_err(|e| self.annotate_error(e))?;

        Ok(())
    }
}

impl<'a> Walker for parser::BlockIter<'a>
{
    fn walk(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        let mut block_handler = BlockHandler::new(self.parser());

        while let Some(block) = self.next()
        {
            block_handler.handle(visitor, block?)
                .map_err(|e| self.annotate_error(e))?;
        }

        block_handler.end_all(visitor)
            .map_err(|e| self.annotate_error(e))?;

        Ok(())
    }
}
