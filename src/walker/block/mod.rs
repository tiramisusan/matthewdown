//!
//! ```text
//! Group Region
//!  |
//!  | Block Region
//!  |  |
//!  |  | Inline Region
//!  |  |  |
//!  |  |  | ... inline content
//!  |  |
//!  |  | ... child blocks
//!  |
//!  | Block Region
//!  |  |
//!  |  | Inline Region
//!  |  |  |
//!  |  |  | ... inline content
//!  |  |
//!  |  | ... child blocks
//!  |
//! ```
//!
//!

use super::*;

mod handler;
pub use handler::BlockHandler;

pub trait Block
{
    /// The 'group' region for this block.
    ///
    /// This block will be placed in a region of this type, which will be
    /// expanded to include sibling blocks of the same group region type.
    ///
    fn group_region(&self) -> Option<visitor::Region> { None }

    /// The 'block' region for this block.
    ///
    /// This block will be placed in a region of this type, along with any
    /// children.
    ///
    fn block_region(&self) -> Option<visitor::Region> { None }

    /// The 'inline' region for this block.
    ///
    /// The inline content of this block will be placed in a region of this
    /// type, but none of its children will.
    ///
    fn inline_region(&self) -> Option<visitor::Region> { None }

    /// Set attributes on this block
    ///
    /// # Parameters
    ///
    ///  * `key` - The name of the attribute to set.
    ///  * `value` - The value of the attribute to set.
    ///
    fn set_attr(&mut self, key: &str, value: &str) -> Result
    {
        let _ = key;
        let _ = value;
        Err(Error::from("Attrs not supported for this block"))
    }

    /// Perform an action after the inline content of this block.
    ///
    /// # Parameters
    ///
    ///  * `visitor` - The visitor to visit the actions upon.
    ///  * `handler` - The current block handling context.
    ///
    fn visit_post(&self, visitor: &mut dyn Visitor, handler: &mut BlockHandler)
        -> Result
    {
        let _ = visitor;
        let _ = handler;

        Ok(())
    }
}

impl<'a> Block for Box<dyn Block + 'a>
{
    fn group_region(&self)  -> Option<visitor::Region>
    {
        self.as_ref().group_region()
    }

    fn block_region(&self)  -> Option<visitor::Region>
    {
        self.as_ref().block_region()
    }

    fn inline_region(&self) -> Option<visitor::Region>
    {
        self.as_ref().inline_region()
    }

    fn set_attr(&mut self, key: &str, value: &str) -> Result
    {
        self.as_mut().set_attr(key, value)
    }

    fn visit_post(&self, visitor: &mut dyn Visitor, handler: &mut BlockHandler)
        -> Result
    {
        self.as_ref().visit_post(visitor, handler)
    }
}

