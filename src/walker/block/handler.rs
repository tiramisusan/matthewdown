use super::*;

use std::collections::HashMap;

/// A structure containing information about a block.
#[derive(Clone)]
struct BlockInfo
{
    /// The depth of this block.
    depth:  usize,
    /// The currently open group region, if any.
    group:  Option<visitor::Region>,
    /// The currently open block region, if any.
    block:  Option<visitor::Region>
}

impl<'a> From<&blocks::Container<'a>> for BlockInfo
{
    fn from(b: &blocks::Container<'a>) -> BlockInfo
    {
        BlockInfo {
            depth:  b.depth(),
            group:  b.block().group_region(),
            block:  b.block().block_region(),
        }
    }
}

/// A struct to handle walking through blocks.
pub struct BlockHandler<'a>
{
    parser: &'a parser::Parser,

    /// A stack of currently walked blocks.
    ///
    /// Blocks can be nested inside one another, and this stack contains the
    /// current stack of blocks being walked through.
    ///
    stack: Vec<BlockInfo>,

    /// A map of attributes to apply to the next block.
    attrs: HashMap<String, String>
}

/// Visit the end of a region.
fn visitor_end(visitor: &mut dyn Visitor, region: &Option<visitor::Region>)
    -> Result
{
    if let Some(r) = region
    {
        visitor.end(r)?;
    }

    Ok(())
}

/// Visit the start of a region.
fn visitor_begin(visitor: &mut dyn Visitor, region: &Option<visitor::Region>)
    -> Result
{
    if let Some(r) = region
    {
        visitor.begin(r)?;
    }

    Ok(())
}

impl<'a> BlockHandler<'a>
{
    pub fn new(parser: &'a parser::Parser) -> Self
    {
        Self
        {
            parser,
            stack: Default::default(),
            attrs: Default::default()
        }
    }

    /// Begin a group region
    fn begin_group(&mut self, visitor: &mut dyn Visitor, info: &BlockInfo)
        -> Result
    {
        visitor_begin(visitor, &info.group)?;
        self.stack.push(
            BlockInfo {
                depth:  info.depth.clone(),
                group:  info.group.clone(),
                block:  None
            }
        );

        Ok(())
    }

    /// End the current group region.
    fn end_group(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        if let Some(info) = self.stack.pop()
        {
            visitor_end(visitor, &info.group)?;
        }

        Ok(())
    }

    /// Begin a new block region.
    fn begin_block(&mut self, visitor: &mut dyn Visitor, info: &BlockInfo)
        -> Result
    {
        visitor_begin(visitor, &info.block)?;

        if let Some(last) = self.stack.last_mut()
        {
            last.block = info.block.clone();
        }

        Ok(())
    }

    /// End the current block region.
    fn end_block(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        if let Some(last) = self.stack.last_mut()
        {
            visitor_end(visitor, &last.block.take())?;
        }

        Ok(())
    }

    /// Begin a new block.
    ///
    /// This will begin a new group and block region, ending any current block
    /// or group regions if necessary.
    ///
    fn begin(&mut self, visitor: &mut dyn Visitor, info: &BlockInfo)
        -> Result
    {
        while let Some(peeked) = self.stack.last()
        {
            if peeked.depth < info.depth
            {
                // Case 1 - we are the most indented block.
                //
                // In this case, we create a new group and new block inside the
                // old last one.
                self.begin_group(visitor, info)?;
                self.begin_block(visitor, info)?;
                return Ok(());
            }
            else if peeked.depth == info.depth && peeked.group == info.group
            {
                // Case 2 - we are a sibling to the last block, and belong in
                //          the same group.
                //
                // In this case, we end the current block, and begin a new one
                // in the same group.
                self.end_block(visitor)?;
                self.begin_block(visitor, info)?;
                return Ok(());
            }
            else
            {
                // Case 3 - we are less indented than the last block, or we
                //          don't belong in the same group.
                //
                // We end the last block, and its containing group, and
                // continue to search for something we can be a child of.
                self.end_block(visitor)?;
                self.end_group(visitor)?;
            }
        }

        // Case 4 - There is no block before us.
        //
        // In this case, we create a new group and begin our item.
        self.begin_group(visitor, info)?;
        self.begin_block(visitor, info)?;

        Ok(())
    }

    /// Handle a block.
    ///
    /// Begin the block, visit the inline content, and any injected content.
    ///
    pub(crate) fn handle(
        &mut self,
        visitor:   &mut dyn Visitor,
        mut block: blocks::Container
    )
        -> Result
    {
        for (k, v) in self.attrs.drain()
        {
            block.block_mut().set_attr(&k, &v)?;
        }

        let info = BlockInfo::from(&block);

        self.begin(visitor, &info)?;

        if let Some(content) = block.content()
        {
            visitor_begin(visitor, &block.block().inline_region())?;
            self.parser.iter_inline(content)
                .walk(visitor)?;
            visitor_end(visitor, &block.block().inline_region())?;
        }

        block.block().visit_post(visitor, self)?;

        Ok(())
    }

    /// Inject a string containing a Matthewdown document.
    ///
    /// Any currently open blocks will be closed, and the contents of the string
    /// will be injected.
    ///
    pub fn inject(&mut self, visitor: &mut dyn Visitor, md: &str) -> Result
    {
        self.end_all(visitor)?;
        self.parser.parse_document(md)?.walk(visitor)?;

        Ok(())
    }

    pub fn set_next_block_attr(&mut self, key: &str, value: &str)
    {
        self.attrs.insert(key.to_owned(), value.to_owned());
    }

    /// End all current blocks.
    ///
    /// This function will clean up all current block and group regions. It
    /// should be called when the `BlockHandler` has reached the end of the
    /// input.
    ///
    pub fn end_all(&mut self, visitor: &mut dyn Visitor) -> Result
    {
        while self.stack.len() > 0
        {
            self.end_block(visitor)?;
            self.end_group(visitor)?;
        }

        Ok(())
    }
}
