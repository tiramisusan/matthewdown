use super::*;

#[derive(Debug)]
pub enum Data
{
    Str(String),
    List(Vec<Data>),
    Map(HashMap<String, Data>)
}

impl de::Error for Error
{
    fn custom<T>(msg: T) -> Self
        where T: std::fmt::Display
    {
        Error::from(format!("{}", msg))
    }
}

impl<'a, 'de> de::IntoDeserializer<'de, Error> for &'a Data
{
    type Deserializer = Self;

    fn into_deserializer(self) -> Self { self }
}

impl<'de> de::Deserializer<'de> for &Data
{
    type Error = Error;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value>
        where V: de::Visitor<'de>
    {
        use de::value::{SeqDeserializer, MapDeserializer};
        match self
        {
            Data::Str(s)  =>
                visitor.visit_str(s),
            Data::List(l) =>
                visitor.visit_seq(SeqDeserializer::new(l.iter())),
            Data::Map(m) =>
                visitor.visit_map(MapDeserializer::new(
                    m.iter()
                        .map(|(k, v)| (k.as_str(), v))
                ))
        }
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
        where V: de::Visitor<'de>
    {
        match self
        {
            Data::Str(s) if s == "true" =>
                visitor.visit_bool(true),
            Data::Str(s) if s == "false" =>
                visitor.visit_bool(false),
            _ =>
                Err(Error::from(format!("Expected bool, found: {:?}", self)))
        }
    }

    serde::forward_to_deserialize_any!(
        i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf option unit unit_struct newtype_struct seq tuple
        tuple_struct map struct enum identifier ignored_any
    );
}
