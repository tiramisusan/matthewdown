use super::*;

use serde::{de};

use std::collections::HashMap;

mod parser;
use parser::Parser;

mod data;
use data::Data;

/// A Matthewdown visitor that extracts all the metadata from a document.
///
/// An inner Matthewdown visitor, `V`, handles all the non-metadata content
/// in the document, while the `MetadataVisitor` consumes all of the metadata
/// in the document.
///
/// A `MetadataVisitor` can be created by calling the `MeatadataVisitor::from`
/// method on an inner Matthewdown visitor. It can then be used to visit a
/// Matthewdown document. The `MetadataVisitor::deserialize_metadata` can then
/// be used to deserialize any metadata into a type.
///
/// # Examples
///
/// Metadata is deserialized using the `serde` library, allowing it to be
/// extracted to custom structures.
///
/// ```
/// use matthewdown::{NullVisitor, MetadataVisitor, Visitor};
///
/// #[derive(serde::Deserialize)]
/// struct Data
/// {
///     apple_shape: String,
///     banana_shape: String
/// }
///
/// let data: Data = MetadataVisitor::from(NullVisitor)
///     .with_visited_str(r#"
/// $$metadata
///   * `apple_shape` - `round`
///   * `banana_shape` - `long`"#)
///     .unwrap()
///     .deserialize_metadata()
///     .unwrap();
///
/// assert_eq!(data.apple_shape, "round");
/// assert_eq!(data.banana_shape, "long");
/// ```
///
/// The `MetadataVisitor` sits on top of another visitor, allowing metadata and
/// content to be extracted in a single pass.
///
/// ```
/// use matthewdown::{HtmlVisitor, MetadataVisitor, Visitor};
///
/// let visitor = MetadataVisitor::from(HtmlVisitor::new())
///     .with_visited_str(r#"
///  Content
///
///  $$metadata
///    1. `One`
///    2. `Two`"#)
///     .unwrap();
///
/// let data: Vec<String> = visitor.deserialize_metadata().unwrap();
/// let html = visitor.into_inner().into_output();
///
/// assert_eq!(data, vec!["One".to_owned(), "Two".to_owned()]);
/// assert_eq!(html, "<p>Content</p>");
/// ```
///
pub struct MetadataVisitor<V: ?Sized>
{
    in_metadata: usize,
    parser:      Parser,
    inner:       V
}

impl<V> From<V> for MetadataVisitor<V>
{
    fn from(inner: V) -> MetadataVisitor<V>
    {
        Self { in_metadata: 0, parser: Parser::default(), inner }
    }
}

impl<V> MetadataVisitor<V>
{
    /// Deserialize the metadata collected while visiting the matthewdown.
    ///
    /// This must be called after visiting is completed.
    ///
    pub fn deserialize_metadata<'de, M>(&self) -> Result<M>
        where M: serde::Deserialize<'de>
    {
        if let Some(data) = self.parser.get_data()
        {
            M::deserialize(data)
        }
        else
        {
            Err(Error::from("No metadata found"))
        }
    }

    /// Get the inner visitor.
    pub fn into_inner(self) -> V { self.inner }
}

impl<V> Visitor for MetadataVisitor<V>
    where V: Visitor
{
    fn begin(&mut self, region: &Region) -> Result
    {
        if region == &Region::Metadata
        {
            self.in_metadata += 1;
        }

        if self.in_metadata > 0
        {
            self.parser.handle_begin(region)
        }
        else
        {
            self.inner.begin(region)
        }
    }

    fn end(&mut self, region: &Region) -> Result
    {
        if region == &Region::Metadata
        {
            self.in_metadata -= 1;
        }

        if self.in_metadata > 0
        {
            self.parser.handle_end(region)
        }
        else
        {
            self.inner.end(region)
        }
    }

    fn text(&mut self, text: &str) -> Result
    {
        if self.in_metadata > 0
        {
            Ok(())
        }
        else
        {
            self.inner.text(text)
        }
    }

    fn line_break(&mut self) -> Result
    {
        if self.in_metadata > 0
        {
            Ok(())
        }
        else
        {
            self.inner.line_break()
        }
    }

    fn code(&mut self, text: &str) -> Result
    {
        if self.in_metadata == 0
        {
            self.inner.code(text)
        }
        else
        {
            self.parser.handle_code(text)
        }
    }
}
