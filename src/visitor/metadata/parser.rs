use super::*;

#[derive(Debug)]
enum Incomplete
{
    List(Vec<Data>),
    Map(HashMap<String, Data>),
    ExpectMapKey,
    ExpectMapValue(String),
    ExpectMapEnd(String, Data),
    ExpectListValue,
    ExpectListEnd(Data)
}

#[derive(Default)]
pub struct Parser
{
    stack: Vec<Incomplete>,
    data:  Option<Data>,
}

impl Parser
{
    fn peek_mut(&mut self) -> Result<&mut Incomplete>
    {
        self.stack.last_mut()
            .ok_or_else(|| Error::from("Expected to be in an element"))
    }

    fn pop(&mut self) -> Result<Incomplete>
    {
        self.stack.pop()
            .ok_or_else(|| Error::from("Expected to be in an element"))
    }

    fn push(&mut self, incom: Incomplete)
    {
        self.stack.push(incom);
    }

    fn handle_data(&mut self, data: Data) -> Result
    {
        if self.stack.len() == 0
        {
            if self.data.is_some()
            {
                return Err(Error::from("Found multiple metadata"));
            }

            self.data = Some(data);
        }
        else
        {
            match self.pop()?
            {
                Incomplete::ExpectMapValue(key) =>
                    self.push(Incomplete::ExpectMapEnd(key, data)),
                Incomplete::ExpectListValue =>
                    self.push(Incomplete::ExpectListEnd(data)),
                _ =>
                    return Err(Error::from("Unexpected data"))
            }
        }

        Ok(())
    }

    fn handle_map_item(&mut self, item: (String, Data)) -> Result
    {
        match self.peek_mut()?
        {
            Incomplete::Map(map) =>
                { map.insert(item.0, item.1); }
            _ =>
                return Err(Error::from("Unexpected map item"))
        }

        Ok(())
    }

    fn handle_list_item(&mut self, item: Data) -> Result
    {
        match self.peek_mut()?
        {
            Incomplete::List(list) =>
                list.push(item),
            _ =>
                return Err(Error::from("Unexpectec list item"))
        }

        Ok(())
    }

    pub fn handle_begin(&mut self, region: &Region) -> Result
    {
        match region
        {
            Region::UnorderedList => self.push(Incomplete::Map(HashMap::new())),
            Region::OrderedList   => self.push(Incomplete::List(Vec::new())),
            Region::UnorderedItem => self.push(Incomplete::ExpectMapKey),
            Region::OrderedItem   => self.push(Incomplete::ExpectListValue),
            _ => { }
        }

        Ok(())
    }

    pub fn handle_end(&mut self, region: &Region) -> Result
    {
        match region
        {
            Region::UnorderedList | Region::OrderedList =>
            {
                let data = match self.pop()?
                    {
                        Incomplete::List(list) => Data::List(list),
                        Incomplete::Map(map)   => Data::Map(map),
                        _ =>
                            return Err(Error::from("Ended list unexpectedly"))
                    };
                self.handle_data(data)?;
            }
            Region::UnorderedItem =>
            {
                let data = match self.pop()?
                    {
                        Incomplete::ExpectMapEnd(key, val) => (key, val),
                        _ =>
                            return Err(
                                Error::from("Ended list item unexpectedly")
                            )
                    };
                self.handle_map_item(data)?;
            }
            Region::OrderedItem =>
            {
                let data = match self.pop()?
                    {
                        Incomplete::ExpectListEnd(val) => val,
                        _ =>
                            return Err(
                                Error::from("Ended list item unexpectedly")
                            )
                    };
                self.handle_list_item(data)?;
            }
            _ => { }
        }

        Ok(())
    }

    pub fn handle_code(&mut self, code: &str) -> Result
    {
        let owned = String::from(code);

        let peeked = self.peek_mut()?;

        if matches!(peeked, Incomplete::ExpectMapKey)
        {
            *peeked = Incomplete::ExpectMapValue(owned);

            Ok(())
        }
        else
        {
            self.handle_data(Data::Str(owned))
        }
    }

    pub fn get_data(&self) -> Option<&Data> { self.data.as_ref() }
}
