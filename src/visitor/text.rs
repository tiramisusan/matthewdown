use super::*;

use std::fmt::Write;

/// A Matthewdown visitor that simply outputs all the text in a document
pub struct TextVisitor<F>(F);

impl<F> Visitor for TextVisitor<F>
    where F: Write
{
    fn begin(&mut self, _region: &Region) -> Result { Ok(()) }
    fn end(&mut self, _region: &Region)   -> Result { Ok(()) }

    fn text(&mut self, text: &str) -> Result { self.0.write_str(text)?; Ok(()) }
    fn line_break(&mut self)       -> Result { self.0.write_char(' ')?; Ok(()) }
    fn code(&mut self, text: &str) -> Result { self.0.write_str(text)?; Ok(()) }
}
