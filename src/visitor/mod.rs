use super::*;

mod html;
pub use html::{HtmlVisitor, HtmlConfig};

mod metadata;
pub use metadata::MetadataVisitor;

mod text;
pub use text::TextVisitor;

mod null;
pub use null::NullVisitor;

#[derive(Clone, PartialEq, Debug)]
pub enum Region
{
    UnorderedList,
    OrderedList,
    UnorderedItem,
    OrderedItem,
    Bold,
    Italic,
    Paragraph,
    Small,
    Nb,
    Heading(usize),
    Link(String),
    Metadata,
    Photo(String),
    CodeBlock,
    BlockQuote,
    Table,
    Cell,
    Row
}

pub trait Visitor
{
    fn begin(&mut self, region: &Region) -> Result;

    fn end(&mut self, region: &Region) -> Result;

    fn text(&mut self, text: &str) -> Result;

    fn line_break(&mut self) -> Result;

    fn code(&mut self, text: &str) -> Result;

    /// Visit a Matthewdown string.
    ///
    /// # Arguments
    ///
    ///  * `s` - The string of Matthewdown to visit.
    ///
    /// # Example
    ///
    /// ```
    /// use matthewdown::Visitor;
    /// let result = matthewdown::HtmlVisitor::new()
    ///     .with_visited_str(r#"
    ///       1. Bullet 1
    ///       2. Bullet 2
    ///     "#)
    ///     .unwrap()
    ///     .into_output();
    ///
    /// assert_eq!(result, "<ol><li>Bullet 1</li><li>Bullet 2</li></ol>");
    /// ```
    ///
    fn with_visited_str(mut self, s: &str) -> Result<Self>
        where Self: Sized
    {
        visit_str(s, &mut self)?;
        Ok(self)
    }

    /// Visit a Matthewdown string using a particular parser.
    ///
    /// # Arguments
    ///
    ///  * `parser` - The parser to use.
    ///  * `s` - The string of Matthewdown to visit.
    ///
    /// # Example
    ///
    /// ```
    /// // Make a normal parser...
    /// let good_parser = matthewdown::Parser::default();
    /// // Make a parser that doesn't understand any parsing rules...
    /// let bad_parser  = matthewdown::Parser::blank();
    ///
    /// use matthewdown::Visitor;
    /// // Using the good parser works
    /// assert!(matthewdown::HtmlVisitor::new()
    ///     .with_parsed_str(&good_parser, "Hello!")
    ///     .is_ok());
    ///
    /// // Using the bad parser doesn't
    /// assert!(matthewdown::HtmlVisitor::new()
    ///     .with_parsed_str(&bad_parser, "Hello!")
    ///     .is_err());
    /// ```
    ///
    fn with_parsed_str(mut self, parser: &parser::Parser, s: &str)
        -> Result<Self>
        where Self: Sized
    {
        parser.visit_str(s, &mut self)?;
        Ok(self)
    }
}

pub struct SizedDyn<'a>(pub &'a mut dyn Visitor);

impl<'a> Visitor for SizedDyn<'a>
{
    fn begin(&mut self, r: &Region) -> Result { self.0.begin(r) }
    fn end(&mut self, r: &Region)   -> Result { self.0.end(r) }
    fn text(&mut self, text: &str)  -> Result { self.0.text(text) }
    fn line_break(&mut self)        -> Result { self.0.line_break() }
    fn code(&mut self, text: &str)  -> Result { self.0.code(text) }
}
