use super::*;

/// A dummy Matthewdown document visitor that doesn't do anything.
pub struct NullVisitor;

impl Visitor for NullVisitor
{
    fn begin(&mut self, _region: &Region) -> Result { Ok(()) }
    fn end(&mut self, _region: &Region)   -> Result { Ok(()) }
    fn text(&mut self, _text: &str)       -> Result { Ok(()) }
    fn line_break(&mut self)              -> Result { Ok(()) }
    fn code(&mut self, _text: &str)       -> Result { Ok(()) }
}
