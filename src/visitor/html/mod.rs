use super::*;

use std::fmt;

mod tag;
pub use tag::HtmlTag;

mod html_write;
pub use html_write::HtmlWrite;

mod formatter;
pub use formatter::Formatter;

/// A structure describing the configuration of an HTML Visitor.
#[derive(Default, Debug)]
#[non_exhaustive]
pub struct HtmlConfig
{
    /// Place `<img>` tags inside `<a>` tags linking to the photo file.
    pub img_link:             bool,
    /// Add a suffix to source URIs in `<img>` tags.
    pub img_thumbnail_suffix: Option<String>
}

impl HtmlConfig
{
    /// Set the `img_link` field to `true`.
    pub fn with_img_link(mut self) -> Self
    {
        self.img_link = true;
        self
    }

    /// Set the `img_thumbnail_suffix` field.
    pub fn with_img_thumnail_suffix(mut self, suffix: &str) -> Self
    {
        self.img_thumbnail_suffix = Some(suffix.to_owned());
        self
    }
}

/// A Matthewdown document visitor that produces HTML output.
#[derive(Debug)]
pub struct HtmlVisitor<F>
{
    output: F,
    config: HtmlConfig
}

impl<F> HtmlVisitor<F>
{
    /// Configure this `HtmlVisitor`.
    ///
    /// # Arguments
    ///
    /// * `config` - The new configuration.
    ///
    /// # Examples
    ///
    /// ```
    /// use matthewdown::{Visitor, HtmlVisitor, HtmlConfig};
    /// let with_link = HtmlVisitor::new()
    ///     .with_config(HtmlConfig::default().with_img_link())
    ///     .with_visited_str("$$photo(cat.png) Caption")
    ///     .unwrap()
    ///     .into_output();
    ///
    /// let no_link = HtmlVisitor::new()
    ///     .with_visited_str("$$photo(cat.png) Caption")
    ///     .unwrap()
    ///     .into_output();
    ///
    /// assert_eq!(with_link, "\
    /// <figure><a href='cat.png' target='_blank'><img src='cat.png' /></a>\
    /// <figcaption>Caption</figcaption></figure>");
    ///
    /// assert_eq!(no_link, "\
    /// <figure><img src='cat.png' />\
    /// <figcaption>Caption</figcaption></figure>");
    /// ```
    ///
    pub fn with_config(mut self, config: HtmlConfig) -> Self
    {
        self.config = config;
        self
    }
}

impl<F> HtmlVisitor<F>
{

    /// Create a new `HtmlVisitor` with an `HtmlWrite` output.
    ///
    /// # Arguments
    ///
    /// * `output` - An implementer of `HtmlWrite` where the generated HTML tags
    ///              will be written.
    ///
    pub fn new_with_html_output(output: F) -> Self
    {
        Self { output, config: Default::default() }
    }

    /// Get the HTML writer from this `HtmlVisitor`.
    pub fn into_html_write(self) -> F { self.output }
}

impl<F> HtmlVisitor<Formatter<F>>
{
    /// Create a new `HtmlVisitor` with an output stream.
    ///
    /// Any generated HTML will be sent to the output stream.
    ///
    /// # Arguments
    ///
    /// * `output` - An output stream for the generated HTML.
    ///
    /// # Examples
    ///
    /// ```
    /// use matthewdown::Visitor;
    /// let mut s = String::new();
    /// matthewdown::HtmlVisitor::new_with_output(&mut s)
    ///     .with_visited_str("**bold**")
    ///     .unwrap()
    ///     .into_output();
    ///
    /// assert_eq!(s, "<p><b>bold</b></p>");
    /// ```
    ///
    pub fn new_with_output(output: F) -> Self
    {
        Self::new_with_html_output(Formatter(output))
    }

    /// Get the output from this `HtmlVisitor`.
    pub fn into_output(self) -> F { self.into_html_write().0 }
}

impl HtmlVisitor<Formatter<String>>
{
    /// Create a new `HtmlVisitor` that appends its output to a string.
    ///
    /// # Examples
    ///
    /// ```
    /// use matthewdown::Visitor;
    /// let s = matthewdown::HtmlVisitor::new()
    ///     .with_visited_str("# Title\n\nContent")
    ///     .unwrap()
    ///     .into_output();
    ///
    /// assert_eq!(s, "<h1>Title</h1><p>Content</p>");
    /// ```
    ///
    pub fn new() -> Self
    {
        Self::new_with_output(String::new())
    }
}

impl<F> HtmlVisitor<F>
    where F: HtmlWrite
{
    fn begin_photo(&mut self, href: &str) -> Result
    {
        let a_tag = HtmlTag::new("a")
            .with_attr("href", href)
            .with_attr("target", "_blank");

        let img_tag = HtmlTag::new("img")
            .with_attr(
                "src",
                match &self.config.img_thumbnail_suffix
                {
                    Some(suffix) => href.to_owned() + suffix,
                    None         => href.to_owned()
                }
            );

        self.output.open(&HtmlTag::new("figure"))?;

        if self.config.img_link
        {
            self.output.open(&a_tag)?;
            self.output.unpaired(&img_tag)?;
            self.output.close(&a_tag)?;
        }
        else
        {
            self.output.unpaired(&img_tag)?;
        }

        self.output.open(&HtmlTag::new("figcaption"))?;

        Ok(())
    }

    fn end_photo(&mut self) -> Result
    {
        self.output.close(&HtmlTag::new("figcaption"))?;
        self.output.close(&HtmlTag::new("figure"))?;

        Ok(())
    }
}

impl<F> Visitor for HtmlVisitor<F>
    where F: HtmlWrite
{
    fn begin(&mut self, region: &Region) -> Result
    {
        // If the region refers to a simple HTML tag, open it.
        if let Some(tag) = region_tag(region)
        {
            self.output.open(&tag)?;
        }

        // Special handling for $$photo
        if let Region::Photo(href) = region
        {
            self.begin_photo(href)?;
        };

        Ok(())
    }

    fn end(&mut self, region: &Region) -> Result
    {
        // If the region refers to a simple HTML tag, end it.
        if let Some(tag) = region_tag(region)
        {
            self.output.close(&tag)?;
        }

        // Special handling for $$photo
        if let Region::Photo(_) = region
        {
            self.end_photo()?;
        }

        Ok(())
    }

    fn text(&mut self, text: &str) -> Result
    {
        self.output.text(text)?;

        Ok(())
    }

    fn line_break(&mut self) -> Result
    {
        self.output.unpaired(&HtmlTag::new("br"))?;

        Ok(())
    }

    fn code(&mut self, text: &str) -> Result
    {
        let code_tag = HtmlTag::new("code");

        self.output.open(&code_tag)?;
        self.output.text(text)?;
        self.output.close(&code_tag)?;

        Ok(())
    }
}

/// Get the HTML tag corresponding to a region.
///
/// Some regions can be simply mapped directly to HTML tags. This function
/// returns the appropriate tag for such regions. Different types of regions may
/// require more complex handling.
fn region_tag(region: &Region) -> Option<HtmlTag>
{
    use Region::*;
    match region
    {
        UnorderedList  => { Some(HtmlTag::new("ul")) }
        OrderedList    => { Some(HtmlTag::new("ol")) }
        UnorderedItem  => { Some(HtmlTag::new("li")) }
        OrderedItem    => { Some(HtmlTag::new("li")) }
        Bold           => { Some(HtmlTag::new("b")) }
        Italic         => { Some(HtmlTag::new("i")) }
        Paragraph      => { Some(HtmlTag::new("p")) }
        Heading(level) => { Some(HtmlTag::new(format!("h{}", level))) }
        CodeBlock      => { Some(HtmlTag::new("pre")) }
        Small          => { Some(HtmlTag::new("small")) }
        Nb             => { Some(HtmlTag::new("aside")) }
        BlockQuote     => { Some(HtmlTag::new("blockquote")) }
        Cell           => { Some(HtmlTag::new("td")) }
        Row            => { Some(HtmlTag::new("tr")) }
        Table          => { Some(HtmlTag::new("table")) }
        Link(href)     => { Some(HtmlTag::new("a").with_attr("href", href)) }
        _ => None
    }
}

