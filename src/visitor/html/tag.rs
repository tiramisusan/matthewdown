use std::fmt;

/// A structure representing an HTML tag.
pub struct HtmlTag
{
    name:  String,
    attrs: Vec<(String, String)>
}

/// Display the tag as its name, followed by a sequence of attributes.
impl fmt::Display for HtmlTag
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        f.write_str(&self.name)?;

        for (k, v) in self.attrs.iter()
        {
            write!(f, " {}='{}'", k, html_escape::encode_quoted_attribute(v))?;
        }

        Ok(())
    }
}

impl HtmlTag
{
    pub fn new(name: impl Into<String>) -> Self
    {
        Self { name: name.into(), attrs: Vec::new() }
    }

    pub fn with_attr(mut self, key: impl Into<String>, value: impl Into<String>)
        -> Self
    {
        self.attrs.push((key.into(), value.into()));
        self
    }

    pub fn name(&self) -> &str { &self.name }

    pub fn attrs(&self) -> impl Iterator<Item=(&str, &str)>
    {
        self.attrs.iter()
            .map(|(a, b)| (a.as_str(), b.as_str()))
    }
}
