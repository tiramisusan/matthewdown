use super::*;

/// A type that converts `HtmlWrite` to `std::fmt::Write`
pub struct Formatter<T>(pub T);

impl<T> HtmlWrite for Formatter<T>
    where T: fmt::Write
{
    fn text(&mut self, s: &str) -> fmt::Result
    {
        self.0.write_str(&html_escape::encode_text(s))
    }

    fn open(&mut self, tag: &HtmlTag) -> fmt::Result
    {
        write!(self.0, "<{}>", tag)
    }

    fn close(&mut self, tag: &HtmlTag) -> fmt::Result
    {
        write!(self.0, "</{}>", tag.name())
    }

    fn unpaired(&mut self, tag: &HtmlTag) -> fmt::Result
    {
        write!(self.0, "<{} />", tag)
    }
}
