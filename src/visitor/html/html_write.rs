use super::*;

/// A trait to be implemented by types that write HTML output.
///
/// An `HtmlVisitor` can be constructed using the
/// `HtmlVisitor::new_with_html_write` function, which allows it to send its
/// output to a type implementing this trait, rather than `std::fmt::Write`.
///
pub trait HtmlWrite
{
    /// Write a text node.
    ///
    /// # Arguments
    ///
    /// * `s` - The text to include in the text node. This is NOT html-escaped.
    ///
    fn text(&mut self, s: &str) -> fmt::Result;

    /// Open an HTML tag.
    ///
    /// i.e. Write `<tag>`.
    ///
    /// # Arguments
    ///
    /// * `tag` - The HTML tag to open.
    ///
    fn open(&mut self, tag: &HtmlTag) -> fmt::Result;

    /// Close an HTML tag.
    ///
    /// i.e. Write `</tag>`.
    ///
    /// # Arguments
    ///
    /// * `tag` - The HTML tag to close.
    ///
    fn close(&mut self, tag: &HtmlTag) -> fmt::Result;

    /// Write an unpaired HTML tag.
    ///
    /// i.e. Write `<tag />`.
    ///
    /// # Arguments
    ///
    /// * `tag` - The HTML tag to write.
    ///
    fn unpaired(&mut self, tag: &HtmlTag) -> fmt::Result;
}
