# TODO List

## Grammar

 [ ] More escape sequences.
   [ ] `\` at the end of a line should prevent the newline from inserting a space,
     and ignore all preceding whitespace.
   [ ] `\n` and friends, and `\x$$`.
 [ ] Citations.
 [ ] Code blocks.
 [ ] More list styles.
 [ ] `>` quote blocks.
 [ ] `*[...]` and `**[...]` blocks for generated content.
 [ ] Commands
   [ ] $$table_of_contents
   [ ] $$sep
   [x] $$metadata
   [ ] notes. $nb[], $$nb ...
   [ ] Images
     [x] $$photo
     [ ] Multiple photos inline

## API

 [ ] Prettier API
 [ ] API for loading direct from paths
 [ ] Visitor.visit(...)
 [x] Unified error structure. enum MdError { Located(pest), Unlocated(String) }
 [ ] HTML don't start at h1
