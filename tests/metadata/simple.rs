use super::*;

use std::collections::HashMap;

#[test]
fn test_simple_unordered()
{
    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `value1`\n",
        "  * `field2` - `value2`\n"
            =>
        HashMap<String, String>,
        [
            ("field1".to_owned(), "value1".to_owned()),
            ("field2".to_owned(), "value2".to_owned())
        ]
            .into_iter().collect()
    );
}

#[test]
fn test_simple_ordered()
{
    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  1. `value1`\n",
        "  2. `value2`\n"
            =>
        Vec<String>,
        vec![String::from("value1"), String::from("value2")]
    );
}

#[test]
fn test_simple_struct()
{
    #[derive(serde::Deserialize, Debug, PartialEq)]
    struct Data
    {
        field1: String,
        field2: String
    }

    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `value1`\n",
        "  * `field2` - `value2`\n"
            =>
        Data,
        Data { field1: String::from("value1"), field2: String::from("value2") }
    );
}

#[test]
fn test_struct_with_vectors()
{
    #[derive(serde::Deserialize, Debug, PartialEq)]
    struct Data
    {
        vec1: Vec<String>,
        vec2: Vec<String>
    }

    test_metadata_deserialize!(
        "$$metadata\n",
        "  * `vec1`\n",
        "    1. `a`\n",
        "    2. `b`\n",
        "  * `vec2`\n",
        "    1. `c`\n",
        "    2. `d`\n"
            =>
        Data,
        Data {
            vec1: vec![String::from("a"), String::from("b")],
            vec2: vec![String::from("c"), String::from("d")]
        }
    );
}
