use super::*;

mod simple;
mod bool;

fn metadata_deserialize<'de, T>(md: &str) -> matthewdown::MdResult<T>
    where T: serde::Deserialize<'de> + 'de
{
    use matthewdown::{MetadataVisitor, NullVisitor, visit_str};
    let mut visitor = MetadataVisitor::from(NullVisitor);

    visit_str(md, &mut visitor)?;

    Ok(visitor.deserialize_metadata::<T>()?)
}

macro_rules! test_metadata_deserialize
{
    ($($md:expr),+ => $type:ty, $expect:expr) =>
    {{
        let md = ::std::concat!($($md),+);

        match metadata_deserialize::<$type>(md)
        {
            Ok(des) =>
            {
                self::assert_eq!(des, $expect)
            },
            Err(err) =>
            {
                ::std::panic!("Error deserializing:\n{}\n{:#?}", err, err)
            }
        }
    }};
}

macro_rules! test_metadata_fails
{
    ($($md:expr),+ => $type:ty) =>
    {{
        let md = ::std::concat!($($md),+);

        match metadata_deserialize::<$type>(md)
        {
            Ok(_) =>
            {
                ::std::panic!("Expected deserialize to fail")
            },
            Err(_) =>
            {
            }
        }
    }};
}

pub(crate) use test_metadata_deserialize;
pub(crate) use test_metadata_fails;
