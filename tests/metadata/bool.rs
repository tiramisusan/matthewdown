use super::*;

use std::collections::HashMap;

#[test]
fn test_simple_bool()
{
    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `true`\n",
        "  * `field2` - `false`\n"
            =>
        HashMap<String, bool>,
        [
            ("field1".to_owned(), true),
            ("field2".to_owned(), false)
        ]
            .into_iter().collect()
    );
}

#[test]
fn test_misspelled_bool()
{
    test_metadata_fails!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `true`\n",
        "  * `field2` - `false`\n",
        "  * `field3` - `salsa`\n"
            =>
        HashMap<String, bool>
    );
}

#[test]
fn test_interpret_bools_as_strings()
{
    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `true`\n",
        "  * `field2` - `false`\n"
            =>
        HashMap<String, String>,
        [
            ("field1".to_owned(), "true".to_owned()),
            ("field2".to_owned(), "false".to_owned())
        ]
            .into_iter().collect()
    );
}

#[test]
fn test_deserialize_bool_in_struct()
{
    #[derive(serde::Deserialize, Debug, PartialEq)]
    struct Data
    {
        field1: bool,
        field2: String
    }

    test_metadata_deserialize!(
        "# Heading\n",
        "$$metadata\n",
        "  * `field1` - `true`\n",
        "  * `field2` - `false`\n"
            =>
        Data,
        Data { field1: true, field2: "false".to_owned() }
    );
}
