// Utilities for building tests
mod utils;
use utils::*;

// Use the pretty diffing for asserts
#[allow(unused_imports)]
use pretty_assertions::{assert_eq, assert_ne};

// Test modules
mod html;
mod metadata;
