use super::*;

#[test]
fn test_simple_links()
{
    test_html_compile!(
        "[link](http://website.com)"
            =>
        "<p><a href='http://website.com'>link</a></p>"
    );
}

