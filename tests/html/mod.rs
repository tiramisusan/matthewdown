use super::*;

mod block;
mod br;
mod code;
mod fonts;
mod links;
mod list;
mod nb;
mod photo;
mod regression;
mod table;

macro_rules! test_html_compile
{
    ($($md:expr),+ => $($html:expr),+) =>
    {{
        let md   = ::std::concat!($($md),+);
        let html = ::std::concat!($($html),+);

        match ::matthewdown::htmlify_str(md)
        {
            Ok(output) =>
            {
                self::assert_eq!(
                    &normalize_whitespace(html),
                    &normalize_whitespace(&output),
                    "Compiled HTML did not match"
                );
            },
            Err(err) =>
            {
                ::std::panic!("Error compiling HTML:\n{}\n{:#?}", err, err)
            }
        }
    }};
}

pub(crate) use test_html_compile;
