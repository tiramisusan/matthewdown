use super::*;

#[test]
fn test_simple_code_block()
{
    test_html_compile!(
        "```\n",
        "code\n",
        "```\n"
            =>
        "<pre><code>code</code></pre>"
    );
}

#[test]
fn test_code_block_source()
{
    test_html_compile!(
        "$$block-attrs(source=source.txt)\n",
        "```\n",
        "code\n",
        "```\n"
            =>
        "<pre><code>code</code></pre><p>",
        "From <a href='source.txt'>source.txt</a>.</p>"
    );
}

#[test]
fn test_simple_inline_code()
{
    test_html_compile!(
        "Text `code` text" => "<p>Text <code>code</code> text</p>"
    );
}
