use super::*;

#[test]
fn test_simple_aside()
{
    test_html_compile!(
        "$$nb This is an aside" => "<aside>This is an aside</aside>"
    );
}

#[test]
fn test_aside_on_new_line()
{
    test_html_compile!(
        "$$nb\n",
        "  This is an aside\n"
            =>
        "<aside>This is an aside</aside>"
    );
}
