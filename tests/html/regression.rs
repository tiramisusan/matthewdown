use super::*;

#[test]
fn test_trailing_eoi_whitespace()
{
    // Previously, this would fail to compile
    test_html_compile!(" " => "");
}

#[test]
fn test_final_trailing_whitespace()
{
    // Previously, this would emit <p>Hello </p>
    test_html_compile!("Hello\n" => "<p>Hello</p>");
}

#[test]
fn test_space_before_link()
{
    // Previously, there was no space before the link.
    test_html_compile!(
        "Hello [link](href)" => "<p>Hello <a href='href'>link</a></p>"
    );
}

#[test]
fn test_list_inside_heading()
{
    // Previously, the list was inside the <h1> element.
    test_html_compile!(
        "# Hello\n * List item" => "<h1>Hello</h1><ul><li>List item</li></ul>"
    );
}

#[test]
fn test_bold_at_start_of_para()
{
    // Previously, the bold would be consumed as the start of a list item
    test_html_compile!(
        "**Bold**" => "<p><b>Bold</b></p>"
    );
}
