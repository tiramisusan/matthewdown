use super::*;

#[test]
fn test_simple_ordered_lists()
{
    test_html_compile!(
        "1. Item 1\n",
        "2. Item 2\n",
        "3. Item 3\n"
            =>
        "<ol>",
        "<li>Item 1</li>",
        "<li>Item 2</li>",
        "<li>Item 3</li>",
        "</ol>"
    );
}

#[test]
fn test_simple_unordered_lists()
{
    test_html_compile!(
        "* Item 1\n",
        "* Item 2\n",
        "* Item 3\n"
            =>
        "<ul>",
        "<li>Item 1</li>",
        "<li>Item 2</li>",
        "<li>Item 3</li>",
        "</ul>"
    );
}

#[test]
fn test_nested_unordered_lists()
{
    test_html_compile!(
        "* Item 1\n",
        "  * Item 2\n"
            =>
        "<ul><li>Item 1",
        "<ul><li>Item 2</li></ul>",
        "</li></ul>"
    );
}

#[test]
fn test_continued_outer_item_in_nested_unordered_lists()
{
    test_html_compile!(
        "* Item 1\n",
        "  * Item 2\n\n",
        "  Item 1 (continued)"
            =>
        "<ul><li>Item 1",
        "<ul><li>Item 2</li></ul>",
        "<p>Item 1 (continued)</p>",
        "</li></ul>"
    );
}

#[test]
fn test_continued_inner_item_in_nested_unordered_lists()
{
    test_html_compile!(
        "* Item 1\n",
        "  * Item 2\n\n",
        "    Item 2 (continued)"
            =>
        "<ul><li>Item 1",
        "<ul><li>Item 2",
        "<p>Item 2 (continued)</p>",
        "</li></ul>",
        "</li></ul>"
    );
}

#[test]
fn test_unordered_list_lorem_ipsum()
{
    test_html_compile!(
        " * ", lorem_ipsum!(), "\n",
        " * ", lorem_ipsum!(), "\n"
            =>
        "<ul>",
        "<li>", lorem_ipsum!(), "</li>",
        "<li>", lorem_ipsum!(), "</li>",
        "</ul>"
    );
}

#[test]
fn test_ordered_list_lorem_ipsum()
{
    test_html_compile!(
        " 1. ", lorem_ipsum!(), "\n",
        " 2. ", lorem_ipsum!(), "\n"
            =>
        "<ol>",
        "<li>", lorem_ipsum!(), "</li>",
        "<li>", lorem_ipsum!(), "</li>",
        "</ol>"
    );
}
