use super::*;

#[test]
fn test_simple_table()
{
    test_html_compile!(
        "$$row\n",
        "  $$cell Top Left\n",
        "  $$cell Top Right\n",
        "$$row\n",
        "  $$cell Bottom Left\n",
        "  $$cell Bottom Right\n"
            =>
        "<table>",
        "<tr><td>Top Left</td><td>Top Right</td></tr>",
        "<tr><td>Bottom Left</td><td>Bottom Right</td></tr>",
        "</table>"
    )
}
