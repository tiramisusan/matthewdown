use super::*;

#[test]
fn test_br()
{
    test_html_compile!(
        "Line 1 $br Line 2" => "<p>Line 1 <br /> Line 2</p>"
    );
}
