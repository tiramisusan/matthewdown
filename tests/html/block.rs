use super::*;

#[test]
fn test_simple_lorem_ipsum()
{
    test_html_compile!(
        "# Hello\n\n",
        lorem_ipsum!(), "\n\n",
        lorem_ipsum!()
            =>
        "<h1>Hello</h1>",
        "<p>", lorem_ipsum!(), "</p>",
        "<p>", lorem_ipsum!(), "</p>"
    )
}

#[test]
fn test_section_lorem_ipsum()
{
    test_html_compile!(
        "# Hello\n\n",
        lorem_ipsum!(), "\n\n",
        "## Hello 2\n\n",
        lorem_ipsum!()
            =>
        "<h1>Hello</h1>",
        "<p>", lorem_ipsum!(), "</p>",
        "<h2>Hello 2</h2>",
        "<p>", lorem_ipsum!(), "</p>"
    );
}



