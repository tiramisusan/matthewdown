use super::*;

#[test]
fn test_simple_photo()
{
    test_html_compile!(
        "$$photo(img.png) I am the caption"
            =>
        "<figure>",
        "<img src='img.png' /><figcaption>I am the caption</figcaption>",
        "</figure>"
    );
}

#[test]
fn test_multiple_photo()
{
    test_html_compile!(
        "$$photo(img.png) I am the caption\n",
        "$$photo(img.png)\n",
        "  I\n",
        "  am\n",
        "  the\n",
        "  caption\n\n",
        "I am a normal paragraph."
            =>
        "<figure>",
        "<img src='img.png' /><figcaption>I am the caption</figcaption>",
        "</figure>",
        "<figure>",
        "<img src='img.png' /><figcaption>I am the caption</figcaption>",
        "</figure>",
        "<p>I am a normal paragraph.</p>"
    );
}
