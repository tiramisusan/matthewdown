use super::*;

#[test]
fn test_italic()
{
    test_html_compile!(
        "Boring *italic* boring" => "<p>Boring <i>italic</i> boring</p>"
    );
}

#[test]
fn test_bold()
{
    test_html_compile!(
        "Boring **bold** boring" => "<p>Boring <b>bold</b> boring</p>"
    );
}

#[test]
fn test_small()
{
    test_html_compile!(
        "$small[Small text]" => "<p><small>Small text</small></p>"
    );
}
