mod whitespace;
//mod assert_eq_diff;
mod lorem_ipsum;

pub(crate) use lorem_ipsum::lorem_ipsum;
//pub use assert_eq_diff::assert_eq_diff;
pub(crate) use whitespace::normalize_whitespace;
