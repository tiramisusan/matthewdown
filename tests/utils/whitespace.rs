pub fn normalize_whitespace(s: &str) -> String
{
    let mut prev = false;
    s.trim()
        .chars()
        .filter_map(|ch| {
            if ch.is_ascii_whitespace()
            {
                if prev { None }
                else { prev = true; Some(' ') }
            }
            else { prev = false; Some(ch) }
        })
        .collect()
}
