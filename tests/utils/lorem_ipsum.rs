macro_rules! lorem_ipsum
{
    () => {
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do\n\
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut\n\
        enim dd minim veniam, quis nostrud exercitation ullamco laboris\n\
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in\n\
        reprehenderit in voluptate velit esse cillum dolore eu fugiat\n\
        nulla pariatur. Excepteur sint occaecat cupidatat non proident,\n\
        sunt in culpa qui officia deserunt mollit anim id est laborum."
    };
}

pub(crate) use lorem_ipsum;
