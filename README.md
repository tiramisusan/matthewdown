# Matthewdown

A new gospel in markdown ripoffs.

This is my markdown-inspired markup language. It is slightly compatible with
Markdown, insofar as a document written in my personal matthewdown style will
compile in both.

I've mainly focused on compiling matthewdown to HTML, but users of the library
can implement their own matthewdown visitors to compile it to any other format.

## The markup language

I've not yet gotten around to writing a specification for matthewdown, but
here are some of the extra features it has:

 * Block (i.e. paragraph) commands prefixed with `$$`.
 * Inline commands prefixed with `$`.
 * Metadata can be embedded in `$$metadata` blocks and easily deserialized using
   the `serde` crate.
 * `$small[...]` command corresponding to HTML's `<small>`.
 * Tables using the `$$row` and `$$cell` commsnds.
 * `$$nb` corresponding to HTML's `<aside>`.

There are some other things that I'd like to add when I can be bothered. They'll
mostly be added when I personally need them, or else, if someone asks me for
them, or sends a pull request:

 * An ability for users of the library to add support for their own custom
   commands.
 * Citations and references.
 * Support for HTML forms.
 * Output to something like `troff`.
