extern crate matthewdown;

use std::io::{self, Read, Write};

fn main() -> io::Result<()>
{
    let mut input = String::new();

    io::stdin().read_to_string(&mut input)?;

    match matthewdown::htmlify_str(&input)
    {
        Ok(output) =>
        {
            io::stdout().write(output.as_bytes())?;
        }
        Err(err) =>
        {
            eprintln!("Error HTMLifying:\n{}", err);
            return Err(io::Error::new(io::ErrorKind::Other, err))
        }
    }

    Ok(())
}
